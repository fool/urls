/************************************************************************
 *                                                                      *
 *    urls by Jordan Sterling 11/16/2010                                *
 *                                                                      *
 *    licensed under: COMMON DEVELOPMENT AND DISTRIBUTION LICENSE       *
 *                    Version 1.0 (CDDL-1.0)                            *
 *                                                                      *
 ************************************************************************
 *
 * Url Encoder
 * Transforms text into percent encoding.
 * There's 2 ways to use this program
 *   1. With arguments:
 *      All arguments are processed as one big string. Multiple args are
 *      treated as a space, and spaces (%20) are added inbetween. So
 *      urls a a
 *      will produce: %61%20%61
 *
 *   2. Without arguments:
 *      Reads from stdin until newline or EOF
 *
 */
#include <stdio.h>

int parseArgs(int argc, char *argv[])
{
        int count = 0;
        while(++count < argc)
        {
                char *input = argv[count];
                while(*input != '\0')
                {
                        printf("%%%x", *input);
                        input++;
                }

                if (count+1 < argc)
                        printf("%%20");
        }
        printf("\n");
}

int main(int argc, char *argv[])
{
        if(argc > 1)
        {
                parseArgs(argc, argv);
                return 0;
        }

        int c = 0;
        while( (c = getchar()) != EOF)
                if(c != '\n' && c != '\r')
                        printf("%%%x", c);
                else
                        break;

        printf("\n");
        return 0;
}

